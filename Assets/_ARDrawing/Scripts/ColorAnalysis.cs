﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorAnalysis : MonoBehaviour
{
    [SerializeField]
    GameObject[] regionsCapture;

    [SerializeField]
    string[] names;

    private Material[] mat;

    private void Awake()
    {
        mat = this.GetComponent<Renderer>().sharedMaterials;
    }

    // Update is called once per frame
    void Update()
    {
        for(int i = 0; i < regionsCapture.Length; i++)
        {
            Texture t = regionsCapture[i].GetComponent<RC_Get_Texture>().RenderCamera.targetTexture;
            changeTexture(t, names[i]);
        }
    }

    private void changeTexture(Texture texture, string name)
    {
        int i = 0;
        switch (name)
        {
            // Rhino :
            case "r_corps":
                i = 0;
                break;
            case "r_ventre":
                i = 1;
                break;
            case "r_machoire":
                i = 2;
                break;
            case "r_tete":
                i = 3;
                break;
            case "r_yeux":
                i = 4;
                break;
            case "r_corne":
                i = 5;
                break;

            // Snake :
            case "s_bouche":
                i = 0;
                break;
            case "s_dos":
                i = 1;
                break;
            case "s_ventre":
                i = 2;
                break;
            case "s_tete":
                i = 3;
                break;
            case "s_yeux":
                i = 4;
                break;

            // Monkey :
            case "m_corps":
                i = 0;
                break;
            case "m_extremites":
                i = 1;
                break;
            case "m_tete":
                i = 2;
                break;
            case "m_nez":
                i = 3;
                break;
            case "m_visage":
                i = 4;
                Debug.Log("Material du visage du singe : " + mat[i].name);
                break;
            case "m_yeux":
                i = 5;
                break;
        }
        mat[i].SetTexture("_MainTex", texture);
    }
}
